# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'url_expander/version'

Gem::Specification.new do |spec|
  spec.name          = "url_expander"
  spec.version       = UrlExpander::VERSION
  spec.authors       = ["Denis Smirnov"]
  spec.email         = ["denis.smirnov@mkechinov.ru"]
  spec.summary       = %q{Expand url to its final form.}
  spec.description   = %q{Automatically expands any given url to its final form, following through multiple redirects.}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

	spec.add_dependency "rspec"
  spec.add_dependency "bundler", "~> 1.2"
  spec.add_dependency "rake"
  spec.add_dependency "addressable"
	spec.add_dependency "nokogiri"
end
