require "url_expander/version"
require "addressable/uri"
require "open-uri"
require "net/http"

module UrlExpander
  def self.expand(url, levels_to_dig = 6)
    # 0. result hash
    data = { :url => nil, :mime => nil, :error => nil, :msg => nil }
    if url != nil && url != ""
      # 1,2. fix & clean
      url = clean_url(fix_url(url))
      # 3. check for skip
      if skip_url?(url)
        data[:msg] = 'Skipping expanded url'
        return data # OUT
      end

      # 4. GO!
      begin
        # Хитрая фиговина, которая делает защиту от двойного encoded (если ссылка пришла уже закодированной)
        # Декодируем, перегоняем в UTF-8, кодируем
        uri = Addressable::URI.parse(URI.encode(URI.decode(url).force_encoding("utf-8")))

        uri.port = 443 if url[0,5] == "https"
        http = Net::HTTP.new(uri.host, uri.port)

        # For debbuging
        # http.set_debug_output $stderr

        if url[0,5] == "https"
          http.use_ssl = true
          # http.ssl_version = "SSLv3"
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
        end

        uri.path = '/' if uri.path == ''
        http.open_timeout = 15
        http.read_timeout = 15

        full_path = uri.path
        full_path = full_path + '?' + uri.query if uri.query != nil
        # в качестве паарметра - относительный путь, хост мы задали при создании http, если передать url - то location будет равен host + url, и при редиректе буждет кривая ссылка
        http.request_get(full_path, 'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.89 Safari/537.1', 'referer' => 'http://google.com') do |response|
          if response && response.code
            # если 200
            if response.kind_of? Net::HTTPSuccess
              link_hrefs = []
              begin
                doc = Nokogiri::HTML(response.body)
                link_nodes = doc.css('link[rel="canonical"]')
                link_hrefs = link_nodes.collect { |node| node[:href] }
              rescue
              end
              url = link_hrefs.first unless link_hrefs.empty?

              data[:url] = url
              data[:error] = nil
              if response.content_type
                data[:mime] = response.content_type
              else
                # зубами блять выдираем майм
                if response.content_length && response.content_length <= 500000
                  if response.body.scan(/\<body\>|\<html\>/).any?
                    data[:mime] = 'text/html'
                  else
                    data[:mime] = nil
                  end
                end
              end
              return data # FINISH
            # если 300
            elsif response.kind_of?(Net::HTTPRedirection) && levels_to_dig != 0
              if response['location']
                location = response['location'].force_encoding('UTF-8')
                # 5. redirect
                # 5.1 check for loop
                if location.scan(/#{uri.host}/).empty?
                  location = "#{uri.port == 443 ? 'https://' : 'http://'}#{uri.host}#{location}"
                end
                if urls_different?(location, url)
                  return expand(location, levels_to_dig - 1) # REDIRECT
                else
                  data[:url] = url
                  data[:error] = nil
                  if response.content_type
                    data[:mime] = response.content_type
                  else
                    # зубами блять выдираем майм
                    if response.content_length && response.content_length <= 500000
                      if response.body.scan(/\<body\>|\<html\>/).any?
                        data[:mime] = 'text/html'
                      else
                        data[:mime] = nil
                      end
                    end
                  end
                  return data # FINISH
                end
              else
                data[:error] = "out. reason: no redirect location."
                return data # OUT
              end
            # если 400 или еще что
            else
              data[:error] = "Sorry, incorrect link. Try another."
              return data # OUT
            end
          else
            data[:error] = "out. reason: bad response."
            return data # OUT
          end
        end
      rescue => e
        data[:error] = "We failed to get url, please verify that provided url is correct"
        return data # OUT
      end
    else
      data[:error] = "No url provided"
      return data # OUT
    end
  end

  # Правим некорректные url
  def self.fix_url(url)
    url = url.strip
    url = "http://#{url}" if !url.scan(/^https?:\/\//).any?
    return url
  end

  # Отчистка
  def self.clean_url(url)
    return nil if url.size > 1020
    # YouTube
    ["&feature=colike", "&feature=youtu.be", "&feature=share", "&feature=player_embedded"].each { |param| url.slice! param } if url.scan(/^https?:\/\/(w{3}\.)?youtube\.com/).any?

    # techcrunch
    ["ncid=rss"].each { |param| url.slice! param } if url.scan(/techcrunch.com/).any?

    # Google Analytics
    [/utm_content=[^&]*/, /utm_campaign=[^&]*/, /utm_medium=[^&]*/, /utm_source=[^&]*/, /utm_term=[^&]*/].each { |param| url.slice! param }

    # Facebook analytics attributes
    [/fb_action_ids=[^&]*/, /fb_action_types=[^&]*/, /fb_source=[^&]*/, /action_object_map=[^&]*/, /action_type_map=[^&]*/, /action_ref_map=[^&]*/].each { |param| url.slice! param }
    [/tu4=[^&]*/].each { |param| url.slice! param } if url.scan(/^https?:\/\/(w{3}\.)?holykaw\.alltop\.com/).any?
    [/partner=[^&]*/].each { |param| url.slice! param } if url.scan(/^https?:\/\/(w{3}\.)?fastcocreate\.com/).any?

    # Hash tags
    url.slice!(/#.*/)

    # Остатки очистки: http://domain.com?a=123&&, http://domain.com?&, http://domain.com?&, http://domain.com?
    [/[&]{2,}/, /\?&$/, /&$/, /\?$/].each { |param| url.slice! param }

    # Удаляем крайний слеш в урлах типа http://www.youtube.com/watch?v=iLx8xlsEBa8\
    url.slice! "\\"

    # Удаляем решетку (js)
    [/fb_action_ids=[^&]*/, /fb_action_types=[^&]*/, /fb_source=[^&]*/, /action_object_map=[^&]*/, /action_type_map=[^&]*/, /action_ref_map=[^&]*/].each { |param| url.slice! param }

    # Удаляем паразитные окончания
    # [/\/$/, /\/\#$/].each { |param| url.slice! param }
    [/\#$/].each { |param| url.slice! param }

    return url
  end

  # Пропускаем ?
  def self.skip_url?(url)
    return true if url.nil?
    return true if url.scan(/^https?:\/\/(w{3}\.)?accounts\.google\.com/).any?
    return true if url.scan(/^https?:\/\/(w{3}\.)?mail\.google\.com/).any?
    return true if url.scan(/^https?:\/\/(w{3}\.)?facebook\.com/).any?
    return true if url.scan(/^https?:\/\/(w{3}\.)?vk\.com\/wall/).any?
    return true if url.scan(/^https?:\/\/(w{3}\.)?foursquare\.com/).any?
    return true if url.scan(/^https?:\/\/(w{3}\.)?gowalla\.com/).any?
    return true if url.scan(/^https?:\/\/(w{3}\.)?twitpic\.com/).any?
    return true if url.scan(/^https?:\/\/(w{3}\.)?4sq\.com/).any?
    return true if url.scan(/^https?:\/\/(w{3}\.)?instagr\.am/).any?
    return true if url.scan(/^https?:\/\/(w{3}\.)?ask\.fm/).any?
  end

  # Однинаковы ли url'ы
  def self.urls_different?(url_a, url_b)
    url_a = clean_url(fix_url(url_a.downcase))
    url_b = clean_url(fix_url(url_b.downcase))
    if url_a == url_b
      return false
    else
      return true
    end
  end
end

