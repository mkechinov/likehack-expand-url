require 'spec_helper'

describe UrlExpander do

	# Проверяем урлы, могут протухнуть
	# http://news.yandex.ru/sport.html
	# http://bit.ly/1fdTniz
	# http://www.ya.ru
	# http://likehack.com/blog/wp-content/uploads/2012/03/likehackbloglogo1.png

	# Normal requests with redirect
	it "should return expanded url for url with http://" do
		url = "http://bit.ly/1fdTniz"
		data = UrlExpander.expand(url)
		data[:url].should == "http://news.yandex.ru/sport.html"
		data[:mime].should == "text/html"
		data[:error].should == nil
	end
	it "should return expanded url for url with http://www." do
		url = "http://www.bit.ly/1fdTniz"
		data = UrlExpander.expand(url)
		data[:url].should == "http://news.yandex.ru/sport.html"
		data[:mime].should == "text/html"
		data[:error].should == nil
	end
	it "should return expanded url for url with www." do
		url = "www.bit.ly/1fdTniz"
		data = UrlExpander.expand(url)
		data[:url].should == "http://news.yandex.ru/sport.html"
		data[:mime].should == "text/html"
		data[:error].should == nil
	end
	it "should return expanded url for url without http://www." do
		url = "bit.ly/1fdTniz"
		data = UrlExpander.expand(url)
		data[:url].should == "http://news.yandex.ru/sport.html"
		data[:mime].should == "text/html"
		data[:error].should == nil
	end

	# Request with bad url
	it "should return crash error for bad url 1" do
		url = "http://bit./1fdTniz"
		data = UrlExpander.expand(url)
		data[:url].should == nil
		data[:mime].should == nil
		data[:error].should == "out. reason: crash on request because of bad url maybe.."
	end
	it "should return crash error for bad url 2" do
		url = "www.bit./1lW4tgX"
		data = UrlExpander.expand(url)
		data[:url].should == nil
		data[:mime].should == nil
		data[:error].should == "out. reason: crash on request because of bad url maybe.."
	end

	# No url
	it "should return crash error for request with empty url" do
		url = ""
		data = UrlExpander.expand(url)
		data[:url].should == nil
		data[:mime].should == nil
		data[:error].should == "out. reason: no url provided."
	end
	it "should return crash error for request with nil url" do
		url = nil
		data = UrlExpander.expand(url)
		data[:url].should == nil
		data[:mime].should == nil
		data[:error].should == "out. reason: no url provided."
	end

	# Page not found
	it "should return 404 error" do
		url = "http://www.ya.ru/qwe2qwe"
		data = UrlExpander.expand(url)
		data[:url].should == nil
		data[:mime].should == nil
		data[:error].should == "out. reason: page not found."
	end

	# Page not found
	it "should return expanded url with 200" do
		url = "http://www.ya.ru"
		data = UrlExpander.expand(url)
		data[:url].should == "http://www.ya.ru"
		data[:mime].should == "text/html"
		data[:error].should == nil
	end

	# Url to picture
	it "should return image mime fo picture url" do
		url = "http://likehack.com/blog/wp-content/uploads/2012/03/likehackbloglogo1.png"
		data = UrlExpander.expand(url)
		data[:url].should == "http://likehack.com/blog/wp-content/uploads/2012/03/likehackbloglogo1.png"
		data[:mime].should == "image/png"
		data[:error].should == nil
	end

end