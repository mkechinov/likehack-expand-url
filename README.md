# UrlExpander

Automatically expands any given url to its final form, following through multiple redirects and handling all errors.

## Installation

Add this line to your application's Gemfile:

    gem "url_expander"

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install url_expander

## Usage

Just choose url you want to expand and pass it into expand method:

	url_you_want_to_expand = "http://bit.ly/1lW4tgX"
	UrlExpander.expand(url_you_want_to_expand)

After expand operation is finished, you will receive hashes like those below.

On success:

	{:url=>"http://lenta.ru/news/2014/03/21/lyskov/", :mime=>"text/html", :error=>nil}

* As you can see, it contains expanded url and mime type.

On failure:

	{:url=>nil, :mime=>nil, :error=>"out. reason: no redirect location."}

* There will be no url or mime, just an error message inside.